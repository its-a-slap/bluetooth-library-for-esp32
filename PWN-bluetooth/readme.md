```
 C:\Users\11868\.platformio\penv\Scripts\platformio.exe run 

```
## LED

```c



```


```
C:\Users\11868\.platformio\penv\Scripts\platformio.exe run --target upload 

```


## PWN

```cpp
#include <Arduino.h>

// 定义 PWM 参数
#define PWM_PIN 12  // 使用 GPIO 12
#define PWM_CHANNEL 0  // 使用 LEDC 通道 0
#define PWM_FREQ 5000  // PWM 频率为 5 kHz
#define PWM_RESOLUTION 8  // PWM 分辨率为 8 位（0-255）

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting PWM Output...");

    // 配置 LEDC PWM
    ledcSetup(PWM_CHANNEL, PWM_FREQ, PWM_RESOLUTION);  // 设置 PWM 通道
    ledcAttachPin(PWM_PIN, PWM_CHANNEL);  // 将 PWM 通道绑定到引脚
}

void loop() {
    // 逐渐增加 PWM 占空比
    for (int dutyCycle = 0; dutyCycle <= 255; dutyCycle++) {
        ledcWrite(PWM_CHANNEL, dutyCycle);  // 设置 PWM 占空比
        Serial.println("Duty Cycle: " + String(dutyCycle));
        delay(20);  // 延时 20ms
    }

    // 逐渐减少 PWM 占空比
    for (int dutyCycle = 255; dutyCycle >= 0; dutyCycle--) {
        ledcWrite(PWM_CHANNEL, dutyCycle);  // 设置 PWM 占空比
        Serial.println("Duty Cycle: " + String(dutyCycle));
        delay(20);  // 延时 20ms
    }
}


```


------


## 成功的代码--2025年2月14日

```cpp
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 BLE 服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE_Server");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 启动服务和广播
    pService->start();
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            Serial.println("Received Data: " + String(value.c_str()));

            // 清空特征值（可选）
            pCharacteristic->setValue("");
        }
    }

    // 延时
    delay(100);
}

```





###
---
```cpp
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 BLE 服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 启动服务和广播
    pService->start();
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}
int Pwn = 0;
void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            Serial.println("Received Data: " + String(value.c_str()));
            int Pwn = String(value.c_str()).toInt();

          
            // 清空特征值（可选）
            pCharacteristic->setValue("");
        }
    }
    Serial.println("Received Data: " + Pwn);
    //
    // 延时
    delay(100);
}  为什么  Serial.println("Received Data: " + Pwn);  的pwn的值不保持

```
问题的根本原因是你在 `loop()` 函数中错误地重新声明了 `Pwn` 变量，导致它的作用域仅限于 `if` 语句块内。因此，外部的 `Pwn` 变量并没有被更新，而是每次都在 `if` 语句块内创建了一个新的局部变量 `Pwn`。

---

### 问题分析

以下是问题代码片段：

```cpp
if (pCharacteristic->getValue().length() > 0) {
    std::string value = pCharacteristic->getValue();
    Serial.println("Received Data: " + String(value.c_str()));
    int Pwn = String(value.c_str()).toInt(); // 这里重新声明了 Pwn
    pCharacteristic->setValue("");
}
```

- `int Pwn = String(value.c_str()).toInt();` 这一行在 `if` 语句块内重新声明了一个局部变量 `Pwn`，而不是更新外部的全局变量 `Pwn`。
- 因此，外部的 `Pwn` 变量始终保持初始值 `0`。

---

### 修复方法

你需要去掉 `int` 关键字，直接更新全局变量 `Pwn`。以下是修复后的代码片段：

```cpp
if (pCharacteristic->getValue().length() > 0) {
    std::string value = pCharacteristic->getValue();
    Serial.println("Received Data: " + String(value.c_str()));
    Pwn = String(value.c_str()).toInt(); // 更新全局变量 Pwn
    pCharacteristic->setValue("");
}
```

---

### 修复后的完整代码

以下是修复后的完整代码：

```cpp
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 BLE 服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 启动服务和广播
    pService->start();
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            Serial.println("Received Data: " + String(value.c_str()));
            Pwn = String(value.c_str()).toInt(); // 更新全局变量 Pwn

            // 清空特征值（可选）
            pCharacteristic->setValue("");
        }
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);

    // 延时
    delay(100);
}
```

---

### 代码说明

1. **全局变量 `Pwn`**：
   - `int Pwn = 0;` 是一个全局变量，用于存储从 BLE 接收到的数据。
   - 在 `loop()` 中，每次接收到新数据时，更新 `Pwn` 的值。

2. **更新 `Pwn`**：
   - 在 `if` 语句块中，使用 `Pwn = String(value.c_str()).toInt();` 更新全局变量 `Pwn`。

3. **打印 `Pwn`**：
   - 在 `loop()` 的末尾，使用 `Serial.print` 和 `Serial.println` 打印当前的 `Pwn` 值。

---

### 测试说明

1. **连接 BLE 设备**：
   - 使用手机或其他 BLE 设备连接到 `ESP32C3_BLE`。
   - 找到特征 `beb5483e-36e1-4688-b7f5-ea07361b26a8`，并向其写入一个整数（例如 `123`）。

2. **串口输出**：
   - 如果发送 `123`，串口监视器会显示：
     ```
     Received Data: 123
     Current Pwn: 123
     ```
   - 如果没有新数据，`Pwn` 的值会保持不变，串口会持续打印当前的 `Pwn` 值。

---

### 注意事项

- **数据格式**：
  - 确保发送的数据是整数格式（例如 `123`），否则 `toInt()` 会返回 `0`。

- **清空特征值**：
  - 如果你不清空特征值（`pCharacteristic->setValue("")`），每次读取都会得到相同的数据。

- **性能优化**：
  - 如果数据量较大或需要更高的实时性，建议使用回调函数而不是轮询。

---

如果有其他问题，请随时提问！


------

## 要在 ESP32 上接收 十六进制数据
```c
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 16 位 UUID 的服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 设置特征的初始值
    pCharacteristic->setValue("Hello BLE!");

    // 启动服务
    pService->start();

    // 设置广播数据
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setName("MySimpleBLE");  // 设置设备名称
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

// 全局变量，用于存储接收到的十六进制数据
uint8_t receivedData[20]; // 假设最大接收长度为 20 字节
size_t receivedDataLength = 0;
int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            receivedDataLength = value.length();

            // 将数据复制到 receivedData 数组中
            memcpy(receivedData, value.data(), receivedDataLength);

            // 打印接收到的十六进制数据
            Serial.print("Received Data (Hex): ");
            for (size_t i = 0; i < receivedDataLength; i++) {
                Serial.printf("%02X ", receivedData[i]);
            }
            Serial.println();

            // 更新全局变量 Pwn   把自己字节赋值给pwn
            if (receivedDataLength >= 2) { // 确保至少有两个字节
                Pwn = receivedData[1]; // 直接赋值字节数据
            }

            // 清空特征值（可选）
            pCharacteristic->setValue(nullptr, 0); // 清空特征值
        }
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);

    // 延时
    delay(100);
}

```

## Notify 

从你提供的代码来看，目前并没有启用 **Notify** 特性。Notify 是 BLE 中的一个重要功能，允许客户端订阅特征值的变化通知。当特征值发生变化时，服务器会自动通知客户端，而不需要客户端主动去读取。

如果你想启用 Notify 特性，需要在创建特征时添加 `NIMBLE_PROPERTY::NOTIFY` 属性，并在适当的时候调用 `notify()` 方法向客户端发送通知。

以下是修改后的代码，启用了 Notify 特性：

### 修改后的代码
```cpp
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 16 位 UUID 的服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征，并启用 READ、WRITE 和 NOTIFY 属性
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY
    );

    // 设置特征的初始值
    pCharacteristic->setValue("Hello BLE!");

    // 启动服务
    pService->start();

    // 设置广播数据
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setName("massageDevice");  // 设置设备名称
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

// 全局变量，用于存储接收到的十六进制数据
uint8_t receivedData[20]; // 假设最大接收长度为 20 字节
size_t receivedDataLength = 0;
int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            receivedDataLength = value.length();

            // 将数据复制到 receivedData 数组中
            memcpy(receivedData, value.data(), receivedDataLength);

            // 打印接收到的十六进制数据
            Serial.print("Received Data (Hex): ");
            for (size_t i = 0; i < receivedDataLength; i++) {
                Serial.printf("%02X ", receivedData[i]);
            }
            Serial.println();

            // 更新全局变量 Pwn
            if (receivedDataLength >= 2) { // 确保至少有两个字节
                Pwn = receivedData[1]; // 直接赋值字节数据
            }

            // 发送通知给客户端
            pCharacteristic->notify(receivedData, receivedDataLength);

            // 清空特征值（可选）
            pCharacteristic->setValue(nullptr, 0); // 清空特征值
        }
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);

    // 延时
    delay(100);
}
```

### 关键修改点
1. **启用 Notify 属性**：
   在创建特征时，添加了 `NIMBLE_PROPERTY::NOTIFY` 属性：
   ```cpp
   pCharacteristic = pService->createCharacteristic(
       CHARACTERISTIC_UUID,
       NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY
   );
   ```

2. **发送通知**：
   在接收到新数据后，调用 `notify()` 方法向客户端发送通知：
   ```cpp
   pCharacteristic->notify(receivedData, receivedDataLength);
   ```

3. **客户端订阅**：
   客户端需要启用 Notify 或 Indicate 特性，才能接收到服务器发送的通知。通常，客户端会通过 `setNotifyValue(true)` 来订阅通知。

### 注意事项
- **Notify 和 Indicate 的区别**：
  - Notify：服务器发送通知，客户端不需要确认。
  - Indicate：服务器发送通知，客户端需要确认。
  如果需要更高的可靠性，可以使用 `NIMBLE_PROPERTY::INDICATE`。

- **客户端订阅**：
  确保客户端正确订阅了 Notify 或 Indicate 特性，否则服务器发送的通知不会被接收。

- **数据长度**：
  BLE 协议对单个通知的数据长度有限制（通常是 20 字节）。如果数据较长，需要分多次发送。

通过以上修改，你的 BLE 服务器将能够向客户端发送通知。


## 同时向外发送hello world的数据

```c++
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 16 位 UUID 的服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征，并启用 READ、WRITE 和 NOTIFY 属性
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE | NIMBLE_PROPERTY::NOTIFY
    );

    // 设置特征的初始值
    pCharacteristic->setValue("Hello BLE!");

    // 启动服务
    pService->start();

    // 设置广播数据
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setName("massageDevice");  // 设置设备名称
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

// 全局变量，用于存储接收到的十六进制数据
uint8_t receivedData[20]; // 假设最大接收长度为 20 字节
size_t receivedDataLength = 0;
int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            receivedDataLength = value.length();

            // 将数据复制到 receivedData 数组中
            memcpy(receivedData, value.data(), receivedDataLength);

            // 打印接收到的十六进制数据
            Serial.print("Received Data (Hex): ");
            for (size_t i = 0; i < receivedDataLength; i++) {
                Serial.printf("%02X ", receivedData[i]);
            }
            Serial.println();

            // 更新全局变量 Pwn
            if (receivedDataLength >= 2) { // 确保至少有两个字节
                Pwn = receivedData[1]; // 直接赋值字节数据
            }

            // 清空特征值（可选）
            pCharacteristic->setValue(nullptr, 0); // 清空特征值
        }

        // 定期发送 "Hello World" 数据
        static unsigned long lastSendTime = 0;
        if (millis() - lastSendTime >= 1000) { // 每 1 秒发送一次
            lastSendTime = millis();

            // 设置要发送的数据
            std::string helloWorldData = "Hello World";
            pCharacteristic->setValue(helloWorldData); // 更新特征值
            pCharacteristic->notify((uint8_t*)helloWorldData.data(), helloWorldData.length()); // 发送通知

            Serial.println("Sent: Hello World");
        }
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);

    // 延时
    delay(100);
}

```

## CONTROL

```C
#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 16 位 UUID 的服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"

// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

// 定义 PWM 参数
#define PWM_PIN 12  // 使用 GPIO 12
#define PWM_CHANNEL 0  // 使用 LEDC 通道 0
#define PWM_FREQ 5000  // PWM 频率为 5 kHz
#define PWM_RESOLUTION 8  // PWM 分辨率为 8 位（0-255）
void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");
  // 配置 LEDC PWM
    ledcSetup(PWM_CHANNEL, PWM_FREQ, PWM_RESOLUTION);  // 设置 PWM 通道
    ledcAttachPin(PWM_PIN, PWM_CHANNEL);  // 将 PWM 通道绑定到引脚
    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 设置特征的初始值
    pCharacteristic->setValue("Hello BLE!");

    // 启动服务
    pService->start();

    // 设置广播数据
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setName("MySimpleBLE");  // 设置设备名称
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

// 全局变量，用于存储接收到的十六进制数据
uint8_t receivedData[20]; // 假设最大接收长度为 20 字节
size_t receivedDataLength = 0;
int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            receivedDataLength = value.length();

            // 将数据复制到 receivedData 数组中
            memcpy(receivedData, value.data(), receivedDataLength);

            // 打印接收到的十六进制数据
            Serial.print("Received Data (Hex): ");
            for (size_t i = 0; i < receivedDataLength; i++) {
                Serial.printf("%02X ", receivedData[i]);
            }
            Serial.println();

            // 更新全局变量 Pwn   把自己字节赋值给pwn
            if (receivedDataLength >= 2) { // 确保至少有两个字节
                Pwn = receivedData[1]; // 直接赋值字节数据
            }

            // 清空特征值（可选）
            pCharacteristic->setValue(nullptr, 0); // 清空特征值
        }
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);
 ledcWrite(PWM_CHANNEL, Pwn);  // 设置 PWM 占空比
    // 延时
    delay(100);
}


```