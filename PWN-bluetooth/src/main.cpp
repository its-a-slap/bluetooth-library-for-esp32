#include <Arduino.h>
#include <NimBLEDevice.h>

// 定义 16 位 UUID 的服务和特征
#define SERVICE_UUID        "4fafc201-1fb5-459e-8fcc-c5c9c331914b"
#define CHARACTERISTIC_UUID "beb5483e-36e1-4688-b7f5-ea07361b26a8"
// 定义 LED 引脚
#define LED_PIN 13  // 使用 D13 引脚（通常板载 LED）
// BLE 服务器和特征对象
NimBLEServer* pServer;
NimBLECharacteristic* pCharacteristic;

// 定义 PWM 参数
#define PWM_PIN 12  // 使用 GPIO 12
#define PWM_CHANNEL 0  // 使用 LEDC 通道 0
#define PWM_FREQ 5000  // PWM 频率为 5 kHz
#define PWM_RESOLUTION 8  // PWM 分辨率为 8 位（0-255）
void setup() {
    // 初始化串口
    Serial.begin(115200);
    Serial.println("Starting BLE Server...");
  // 配置 LEDC PWM
    ledcSetup(PWM_CHANNEL, PWM_FREQ, PWM_RESOLUTION);  // 设置 PWM 通道
    ledcAttachPin(PWM_PIN, PWM_CHANNEL);  // 将 PWM 通道绑定到引脚

 // 初始化 LED 引脚为输出模式
    pinMode(LED_PIN, OUTPUT);

    // 初始化 BLE
    NimBLEDevice::init("ESP32C3_BLE");
    pServer = NimBLEDevice::createServer();

    // 创建 BLE 服务
    NimBLEService* pService = pServer->createService(SERVICE_UUID);

    // 创建 BLE 特征
    pCharacteristic = pService->createCharacteristic(
        CHARACTERISTIC_UUID,
        NIMBLE_PROPERTY::READ | NIMBLE_PROPERTY::WRITE
    );

    // 设置特征的初始值
    pCharacteristic->setValue("Hello BLE!");

    // 启动服务
    pService->start();
    // 设置广播数据
    NimBLEAdvertising* pAdvertising = NimBLEDevice::getAdvertising();
    pAdvertising->addServiceUUID(SERVICE_UUID);
    pAdvertising->setName("massageDevice");  // 设置设备名称
    pAdvertising->start();

    Serial.println("BLE Server Started. Waiting for connections...");
}

// 全局变量，用于存储接收到的十六进制数据
uint8_t receivedData[20]; // 假设最大接收长度为 20 字节
size_t receivedDataLength = 0;
int Pwn = 0; // 全局变量，用于存储接收到的数据

void loop() {
    // 检查是否有客户端连接
    if (pServer->getConnectedCount() > 0) {
        // 如果有客户端连接，保持 LED 常亮
        digitalWrite(LED_PIN, HIGH);  // 点亮 LED

        // 检查特征是否有新数据
        if (pCharacteristic->getValue().length() > 0) {
            // 读取数据
            std::string value = pCharacteristic->getValue();
            receivedDataLength = value.length();

            // 将数据复制到 receivedData 数组中
            memcpy(receivedData, value.data(), receivedDataLength);

            // 打印接收到的十六进制数据
            Serial.print("Received Data (Hex): ");
            for (size_t i = 0; i < receivedDataLength; i++) {
                Serial.printf("%02X ", receivedData[i]);
            }
            Serial.println();

            // 更新全局变量 Pwn
            if (receivedDataLength >= 2) { // 确保至少有两个字节
                Pwn = receivedData[1]; // 直接赋值字节数据
            }

            // 清空特征值（可选）
            pCharacteristic->setValue(nullptr, 0); // 清空特征值
        }
    } else {
        // 如果没有客户端连接，让 LED 闪烁
        digitalWrite(LED_PIN, HIGH);  // 点亮 LED
        delay(500);                   // 延时 500 毫秒
        digitalWrite(LED_PIN, LOW);   // 熄灭 LED
        delay(500);                   // 延时 500 毫秒
    }

    // 打印当前的 Pwn 值
    Serial.print("Current Pwn: ");
    Serial.println(Pwn);
    ledcWrite(PWM_CHANNEL, Pwn);  // 设置 PWM 占空比

    // 延时
    delay(100);
}