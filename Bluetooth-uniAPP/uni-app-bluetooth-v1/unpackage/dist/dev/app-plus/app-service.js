if (typeof Promise !== "undefined" && !Promise.prototype.finally) {
  Promise.prototype.finally = function(callback) {
    const promise = this.constructor;
    return this.then(
      (value) => promise.resolve(callback()).then(() => value),
      (reason) => promise.resolve(callback()).then(() => {
        throw reason;
      })
    );
  };
}
;
if (typeof uni !== "undefined" && uni && uni.requireGlobal) {
  const global = uni.requireGlobal();
  ArrayBuffer = global.ArrayBuffer;
  Int8Array = global.Int8Array;
  Uint8Array = global.Uint8Array;
  Uint8ClampedArray = global.Uint8ClampedArray;
  Int16Array = global.Int16Array;
  Uint16Array = global.Uint16Array;
  Int32Array = global.Int32Array;
  Uint32Array = global.Uint32Array;
  Float32Array = global.Float32Array;
  Float64Array = global.Float64Array;
  BigInt64Array = global.BigInt64Array;
  BigUint64Array = global.BigUint64Array;
}
;
if (uni.restoreGlobal) {
  uni.restoreGlobal(Vue, weex, plus, setTimeout, clearTimeout, setInterval, clearInterval);
}
(function(vue) {
  "use strict";
  function formatAppLog(type, filename, ...args) {
    if (uni.__log__) {
      uni.__log__(type, filename, ...args);
    } else {
      console[type].apply(console, [...args, filename]);
    }
  }
  const _export_sfc = (sfc, props) => {
    const target = sfc.__vccOpts || sfc;
    for (const [key, val] of props) {
      target[key] = val;
    }
    return target;
  };
  const _sfc_main$1 = {
    setup() {
      const count = vue.ref(0);
      const content = vue.ref("");
      const blueDeviceList = vue.ref([]);
      function initBlue() {
        uni.openBluetoothAdapter({
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:48", "初始化蓝牙成功");
            formatAppLog("log", "at pages/index/index.vue:49", res);
          },
          fail(err) {
            formatAppLog("log", "at pages/index/index.vue:52", "初始化蓝牙失败");
            formatAppLog("error", "at pages/index/index.vue:53", err);
          }
        });
      }
      function discovery() {
        uni.startBluetoothDevicesDiscovery({
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:62", "开始搜索");
            uni.onBluetoothDeviceFound(found);
          },
          fail(err) {
            formatAppLog("log", "at pages/index/index.vue:67", "搜索失败");
            formatAppLog("error", "at pages/index/index.vue:68", err);
          }
        });
      }
      function found(res) {
        formatAppLog("log", "at pages/index/index.vue:75", res);
        blueDeviceList.value.push(res.devices[0]);
      }
      const deviceId = vue.ref("");
      const serviceId = vue.ref("");
      function connect(data) {
        formatAppLog("log", "at pages/index/index.vue:85", 4, data);
        formatAppLog("log", "at pages/index/index.vue:86", 5, data.advertisServiceUUIDs[0]);
        formatAppLog("log", "at pages/index/index.vue:87", 51, data.deviceId);
        deviceId.value = data.deviceId;
        serviceId.value = data.advertisServiceUUIDs[0];
        uni.createBLEConnection({
          deviceId: deviceId.value,
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:93", "连接成功");
            formatAppLog("log", "at pages/index/index.vue:94", 4, res);
            stopDiscovery();
            uni.showToast({
              title: "连接成功"
            });
          },
          fail(err) {
            formatAppLog("log", "at pages/index/index.vue:102", "连接失败");
            formatAppLog("error", "at pages/index/index.vue:103", err);
            uni.showToast({
              title: "连接成功",
              icon: "error"
            });
          }
        });
      }
      function stopDiscovery() {
        uni.stopBluetoothDevicesDiscovery({
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:116", "停止成功");
            formatAppLog("log", "at pages/index/index.vue:117", res);
          },
          fail(err) {
            formatAppLog("log", "at pages/index/index.vue:120", "停止失败");
            formatAppLog("error", "at pages/index/index.vue:121", err);
          }
        });
      }
      function getServices() {
        uni.getBLEDeviceServices({
          deviceId: deviceId.value,
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:135", 6, res);
            uni.showToast({
              title: "获取服务成功"
            });
          },
          fail(err) {
            formatAppLog("error", "at pages/index/index.vue:143", err);
            uni.showToast({
              title: "获取服务失败",
              icon: "error"
            });
          }
        });
      }
      const characteristicId = vue.ref("");
      function getCharacteristics() {
        setTimeout(() => {
          uni.getBLEDeviceCharacteristics({
            deviceId: deviceId.value,
            serviceId: serviceId.value,
            success(res) {
              formatAppLog("log", "at pages/index/index.vue:163", 7, res);
              const newCharacteristics = res.characteristics.find((item) => {
                if (item.properties.read === true && item.properties.write === true) {
                  return item;
                }
              });
              formatAppLog("log", "at pages/index/index.vue:177", 70, newCharacteristics);
              if (newCharacteristics) {
                characteristicId.value = newCharacteristics.uuid;
                formatAppLog("log", "at pages/index/index.vue:181", 71, characteristicId.value);
                uni.showToast({
                  title: "获取特征值成功"
                });
              } else {
                uni.showToast({
                  title: "未找到符合条件的特征值",
                  icon: "error"
                });
              }
            },
            fail(err) {
              formatAppLog("error", "at pages/index/index.vue:193", err);
              uni.showToast({
                title: "获取特征值失败",
                icon: "error"
              });
            }
          });
        }, 1500);
      }
      function notify() {
        uni.notifyBLECharacteristicValueChange({
          deviceId: deviceId.value,
          // 设备id
          serviceId: serviceId.value,
          // 监听指定的服务
          characteristicId: characteristicId.value,
          // 监听对应的特征值
          state: true,
          //是否启用 notify
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:213", "开启消息监听", res);
            listenValueChange();
            uni.showToast({
              title: "已开启监听"
            });
          },
          fail(err) {
            formatAppLog("error", "at pages/index/index.vue:220", 8, err);
            uni.showToast({
              title: "监听失败",
              icon: "error"
            });
          }
        });
      }
      function ab2hex(buffer) {
        const hexArr = Array.prototype.map.call(
          new Uint8Array(buffer),
          function(bit) {
            return ("00" + bit.toString(16)).slice(-2);
          }
        );
        return hexArr.join("");
      }
      function hexCharCodeToStr(hexCharCodeStr) {
        var trimedStr = hexCharCodeStr.trim();
        var rawStr = trimedStr.substr(0, 2).toLowerCase() === "0x" ? trimedStr.substr(2) : trimedStr;
        var len = rawStr.length;
        if (len % 2 !== 0) {
          alert("存在非法字符!");
          return "";
        }
        var curCharCode;
        var resultStr = [];
        for (var i = 0; i < len; i = i + 2) {
          curCharCode = parseInt(rawStr.substr(i, 2), 16);
          resultStr.push(String.fromCharCode(curCharCode));
        }
        return resultStr.join("");
      }
      const message = vue.ref("");
      const messageHex = vue.ref("");
      function listenValueChange() {
        uni.onBLECharacteristicValueChange((res) => {
          formatAppLog("log", "at pages/index/index.vue:265", "监听结果", res);
          let resHex = ab2hex(res.value);
          formatAppLog("log", "at pages/index/index.vue:268", "resHex", resHex);
          messageHex.value = resHex;
          let result = hexCharCodeToStr(resHex);
          formatAppLog("log", "at pages/index/index.vue:272", "result", String(result));
          message.value = String(result);
        });
      }
      function send() {
        var data = [255, 119, 255];
        formatAppLog("log", "at pages/index/index.vue:315", 13, data);
        var buf = new ArrayBuffer(data.length);
        var dataView = new DataView(buf);
        data.forEach(function(item, index) {
          dataView.setUint8(index, item);
        });
        formatAppLog("log", "at pages/index/index.vue:321", 13, buf);
        uni.writeBLECharacteristicValue({
          deviceId: deviceId.value,
          serviceId: serviceId.value,
          characteristicId: characteristicId.value,
          value: buf,
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:329", "发送写入数据", res);
            uni.showToast({
              title: "write指令发送成功"
            });
          },
          fail(err) {
            formatAppLog("error", "at pages/index/index.vue:335", err);
            uni.showToast({
              title: "write指令发送失败",
              icon: "error"
            });
          }
        });
      }
      function send2() {
        var data = [136, 119, 102, 240, 68, 51, 34, 17];
        var buf = new ArrayBuffer(data.length);
        var dataView = new DataView(buf);
        data.forEach(function(item, index) {
          dataView.setUint8(index, item);
        });
        uni.writeBLECharacteristicValue({
          deviceId: deviceId.value,
          serviceId: serviceId.value,
          characteristicId: characteristicId.value,
          value: buf,
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:361", "发送写入数据", res);
            uni.showToast({
              title: "write指令发送成功"
            });
          },
          fail(err) {
            formatAppLog("error", "at pages/index/index.vue:367", err);
            uni.showToast({
              title: "write指令发送失败",
              icon: "error"
            });
          }
        });
      }
      function read() {
        uni.readBLECharacteristicValue({
          deviceId: deviceId.value,
          serviceId: serviceId.value,
          characteristicId: characteristicId.value,
          success(res) {
            formatAppLog("log", "at pages/index/index.vue:385", "读取数据", res);
            uni.showToast({
              title: "read指令发送成功"
            });
          },
          fail(err) {
            formatAppLog("error", "at pages/index/index.vue:391", err);
            uni.showToast({
              title: "read指令发送失败",
              icon: "error"
            });
          }
        });
      }
      function inputBlur(e) {
        content.value = e.target.value;
        formatAppLog("log", "at pages/index/index.vue:447", 12, content.value);
      }
      return {
        count,
        initBlue,
        discovery,
        getServices,
        getCharacteristics,
        notify,
        send,
        send2,
        read,
        blueDeviceList,
        connect,
        content,
        inputBlur
      };
    },
    mounted() {
      formatAppLog("log", "at pages/index/index.vue:469", this.count);
    }
  };
  function _sfc_render(_ctx, _cache, $props, $setup, $data, $options) {
    return vue.openBlock(), vue.createElementBlock("view", { class: "container" }, [
      vue.createCommentVNode(" 设备列表 "),
      vue.createElementVNode("scroll-view", {
        "scroll-y": "",
        class: "device-list"
      }, [
        (vue.openBlock(true), vue.createElementBlock(
          vue.Fragment,
          null,
          vue.renderList($setup.blueDeviceList, (item) => {
            return vue.openBlock(), vue.createElementBlock("view", {
              class: "device-item",
              key: item.deviceId,
              onClick: ($event) => $setup.connect(item)
            }, [
              vue.createElementVNode(
                "text",
                { class: "device-id" },
                "设备ID: " + vue.toDisplayString(item.deviceId),
                1
                /* TEXT */
              ),
              vue.createElementVNode(
                "text",
                { class: "device-name" },
                "设备名称: " + vue.toDisplayString(item.name || "未知设备"),
                1
                /* TEXT */
              )
            ], 8, ["onClick"]);
          }),
          128
          /* KEYED_FRAGMENT */
        ))
      ]),
      vue.createCommentVNode(" 操作按钮 "),
      vue.createElementVNode("view", { class: "button-group" }, [
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[0] || (_cache[0] = (...args) => $setup.initBlue && $setup.initBlue(...args))
        }, "初始化蓝牙"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[1] || (_cache[1] = (...args) => $setup.discovery && $setup.discovery(...args))
        }, "搜索附近蓝牙设备"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[2] || (_cache[2] = (...args) => $setup.getServices && $setup.getServices(...args))
        }, "获取蓝牙服务"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[3] || (_cache[3] = (...args) => $setup.getCharacteristics && $setup.getCharacteristics(...args))
        }, "获取特征值"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[4] || (_cache[4] = (...args) => $setup.notify && $setup.notify(...args))
        }, "开启消息监听"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[5] || (_cache[5] = (...args) => $setup.send && $setup.send(...args))
        }, "发送数据"),
        vue.createElementVNode("button", {
          class: "btn",
          onClick: _cache[6] || (_cache[6] = (...args) => $setup.read && $setup.read(...args))
        }, "读取数据")
      ]),
      vue.createCommentVNode(" 消息显示区域 "),
      _ctx.message ? (vue.openBlock(), vue.createElementBlock("view", {
        key: 0,
        class: "message-box"
      }, [
        vue.createElementVNode(
          "text",
          { class: "message-text" },
          "监听到的内容：" + vue.toDisplayString(_ctx.message),
          1
          /* TEXT */
        ),
        vue.createElementVNode(
          "text",
          { class: "message-hex" },
          "监听到的内容（十六进制）：" + vue.toDisplayString(_ctx.messageHex),
          1
          /* TEXT */
        )
      ])) : vue.createCommentVNode("v-if", true)
    ]);
  }
  const PagesIndexIndex = /* @__PURE__ */ _export_sfc(_sfc_main$1, [["render", _sfc_render], ["__scopeId", "data-v-1cf27b2a"], ["__file", "E:/1-Git/Project/bluetooth-library-for-esp32/Bluetooth-uniAPP/uni-app-bluetooth-v1/pages/index/index.vue"]]);
  __definePage("pages/index/index", PagesIndexIndex);
  const _sfc_main = {
    onLaunch: function() {
      formatAppLog("log", "at App.vue:4", "App Launch");
    },
    onShow: function() {
      formatAppLog("log", "at App.vue:7", "App Show");
    },
    onHide: function() {
      formatAppLog("log", "at App.vue:10", "App Hide");
    }
  };
  const App = /* @__PURE__ */ _export_sfc(_sfc_main, [["__file", "E:/1-Git/Project/bluetooth-library-for-esp32/Bluetooth-uniAPP/uni-app-bluetooth-v1/App.vue"]]);
  function createApp() {
    const app = vue.createVueApp(App);
    return {
      app
    };
  }
  const { app: __app__, Vuex: __Vuex__, Pinia: __Pinia__ } = createApp();
  uni.Vuex = __Vuex__;
  uni.Pinia = __Pinia__;
  __app__.provide("__globalStyles", __uniConfig.styles);
  __app__._component.mpType = "app";
  __app__._component.render = () => {
  };
  __app__.mount("#app");
})(Vue);
